class Card {
    constructor(id, owner, name, race, power, mana, ability, color, powerAttackAmount = null) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.race = race;
        this.power = power;
        this.mana = mana;
        this.ability = ability;
        this.color = color;
        this.unknown = true;
        this.powerAttackAmount = powerAttackAmount;
    }
}

module.exports = Card;
