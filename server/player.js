const Card = require("./card");
//const cards = require("./cards");
const darkness_cards = require("./cards_darkness");
const light_cards = require("./cards_light");
const fire_cards = require("./cards_fire");
const nature_cards = require("./cards_nature");
const water_cards = require("./cards_water");
class Player {
    constructor(socket) {
        this.socket = socket;
        this.cards = {
            hand: [],
            graveyard: [],
            mana: [],
            battlezone: [],
            deck: [],
            shield: [],
        };
    }
    sendCard(card) {
        if (card.unknown) {
            this.socket.send(JSON.stringify({ event: "sendcard", card: { name: "unknown", id: card.id, owner: card.owner } }));
        } else {
            this.socket.send(JSON.stringify({ event: "sendcard", card: { name: card.name, id: card.id, owner: card.owner } }));
        }
    }
}

module.exports = Player;
