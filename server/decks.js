var p1 = [
    "aqua_hulcus",
    "aqua_knight",
    "aqua_sniper",
    "aqua_soldier",
    "aqua_vehicle",
    "armored_walker_urherion",
    "artisan_picora",
    "astrocomet_dragon",
    "black_feather_shadow_of_rage",
    "bloody_squito",
    "bolshack_dragon",
    "bone_assassin_the_ripper",
    "brawler_zyler",
    "chaos_strike",
    "chilias_the_oracle",
    "creeping_plague",
    "crimson_hammer",
    "crystal_memory",
    "dark_clown",
    "dark_raven_shadow_of_grief",
    "dark_reversal",
    "chaos_strike",
    "chilias_the_oracle",
    "creeping_plague",
    "crimson_hammer",
    "crystal_memory",
    "dark_clown",
    "dark_raven_shadow_of_grief",
    "dark_reversal",
    "chaos_strike",
    "chilias_the_oracle",
    "creeping_plague",
    "crimson_hammer",
    "crystal_memory",
    "dark_clown",
    "dark_raven_shadow_of_grief",
    "dark_reversal",
    "brain_serum",
];

var p2 = [
    "aqua_hulcus",
    "aqua_knight",
    "aqua_sniper",
    "aqua_soldier",
    "aqua_vehicle",
    "armored_walker_urherion",
    "artisan_picora",
    "astrocomet_dragon",
    "aura_blast",
    "black_feather_shadow_of_rage",
    "bloody_squito",
    "bolshack_dragon",
    "bone_assassin_the_ripper",
    "brain_serum",
    "brawler_zyler",
    "bronze_arm_tribe",
    "burning_mane",
    "burning_power",
    "candy_drop",
    "chaos_strike",
    "chilias_the_oracle",
    "coiling_vines",
    "creeping_plague",
    "crimson_hammer",
    "crystal_memory",
    "dark_clown",
    "dark_raven_shadow_of_grief",
    "dark_reversal",
    "stampeding_longhorn",
    "steel_smasher",
    "stinger_worm",
    "stonesaur",
    "storm_shell",
    "super_explosive_volcanodon",
    "swamp_worm",
    "szubs_kin_twilight_guardian",
    "teleportation",
    "terror_pit",
    "thorny_mandra",
    "toel_vizier_of_hope",
    "tornado_flame",
    "tower_shell",
    "tri_horn_shepherd",
    "tropico",
    "ultimate_force",
    "unicorn_fish",
    "urth_purifying_elemental",
    "vampire_silphy",
    "virtual_tripwire",
    "wanderning_braineater",
    "writhing_bone_ghoul",
    "zagaan_knight_of_darkness",
    "phantom_fish",
    "poisonous_dahlia",
    "poisonous_mushroom",
    "rayla_truth_enforcer",
    "red_eye_scorpion",
    "reusol_the_oracle",
    "revolver_fish",
    "roaring_great_horn",
    "rothus_the_traveler",
    "ruby_grass",
    "saucer_head_shark",
    "scarlet_skyterror",
    "seamine",
    "senatine_jade_tree",
    "skeleton_soldier_the_defiled",
    "solar_ray",
    "sonic_wing",
    "spiral_gate",
];

module.exports = {
    p1,
    p2,
};
