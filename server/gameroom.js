const Card = require("./card");
const darkness_cards = require("./cards_darkness");
const light_cards = require("./cards_light");
const fire_cards = require("./cards_fire");
const nature_cards = require("./cards_nature");
const water_cards = require("./cards_water");
class GameRoom {
    constructor(p1 = null, p2 = null) {
        this.p1 = p1;
        this.p2 = p2;
        this.turn = {
            id: null,
            drawsLeft: 1,
            manaToPutLeft: 1,
        };
    }
    init() {
        if (this.p1 != null && this.p2 != null) {
            this.sendPlayers();
            this.sendDecks(this.p1.cards, this.p2.cards);
            /*this.broadcast(
                JSON.stringify({
                    event: "init",
                    p1deck: this.p1.deck,
                    p2deck: this.p2.deck,
                })
            );*/
        }
    }
    sendPlayers() {
        this.broadcast({ event: "receiveplayers", p1: { id: this.p1.socket.id }, p2: { id: this.p2.socket.id } });
    }
    sendDecks(p1deck, p2deck) {
        var p1copyDeck = JSON.parse(JSON.stringify(p1deck));
        var p2copyDeck = JSON.parse(JSON.stringify(p2deck));
        for (var key in p1copyDeck) {
            var array = p1copyDeck[key];
            for (var i = 0; i < array.length; i++) {
                array[i] = { name: "unknown", id: array[i].id, owner: array[i].owner };
            }
        }
        for (var key in p2copyDeck) {
            var array = p2copyDeck[key];
            for (var i = 0; i < array.length; i++) {
                array[i] = { name: "unknown", id: array[i].id, owner: array[i].owner };
            }
        }
        this.broadcast({
            event: "receivedecks",
            p1: { id: this.p1.socket.id, deck: p1copyDeck },
            p2: { id: this.p2.socket.id, deck: p2copyDeck },
        });
    }
    drawCard(id) {
        var player;
        var opponent;
        if (id === this.p1.socket.id) {
            player = this.p1;
            opponent = this.p2;
        } else {
            player = this.p2;
            opponent = this.p1;
        }
        var deck = player.cards["deck"];
        var hand = player.cards["hand"];
        if (deck.length > 0) {
            var temp = deck[deck.length - 1];
            deck.pop();
            hand.push(temp);
            //this.socket.send(JSON.stringify({ event: "movecard", from: "deck", to: "hand", id: temp.id, name: temp.name }));
            player.socket.send(
                JSON.stringify({
                    event: "movecard",
                    playerid: player.socket.id,
                    from: "deck",
                    to: "hand",
                    id: temp.id,
                    name: temp.name,
                })
            );
            opponent.socket.send(
                JSON.stringify({
                    event: "movecard",
                    playerid: player.socket.id,
                    from: "deck",
                    to: "hand",
                    id: temp.id,
                    name: "unknown",
                })
            );
            temp = null;
        }
    }
    generateDecks(p1list, p2list) {
        var cards = {
            ...darkness_cards,
            ...light_cards,
            ...fire_cards,
            ...nature_cards,
            ...water_cards,
        };
        this.p1.cards["deck"] = [];
        this.p2.cards["deck"] = [];
        for (var i = 0; i < p1list.length; i++) {
            var card = cards[p1list[i]];
            this.p1.cards["deck"].push(
                new Card(i, this.p1.socket.id, card.name, card.race, card.power, card.mana, card.ability, card.color)
            );
        }
        for (var i = 0; i < p2list.length; i++) {
            var card = cards[p2list[i]];
            this.p2.cards["deck"].push(
                new Card(i, this.p2.socket.id, card.name, card.race, card.power, card.mana, card.ability, card.color)
            );
        }
    }
    broadcast(data) {
        if (this.p1 != null) this.p1.socket.send(JSON.stringify(data));
        if (this.p2 != null) this.p2.socket.send(JSON.stringify(data));
    }
    getMana(player) {
        return player.cards.mana.length;
    }
}

module.exports = GameRoom;
