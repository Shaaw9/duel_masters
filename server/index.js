const WebSocket = require("ws");
const fs = require("fs");
const uuid = require("uuid");

const Card = require("./card");
const Player = require("./player");
const GameRoom = require("./gameroom");
const log = console.log;
const darkness_cards = require("./cards_darkness");
const light_cards = require("./cards_light");
const fire_cards = require("./cards_fire");
const nature_cards = require("./cards_nature");
const water_cards = require("./cards_water");
const decks = require("./decks");
const port = 8010;

const wss = new WebSocket.Server({ port: port });

log("Server started on port: " + port);

var players = [];
var game = new GameRoom();

wss.on("open", function open() {
    ws.send("something");
});

wss.on("message", function incoming(data) {
    console.log(data);
});
function sendUserInfo(socket) {
    socket.send(JSON.stringify({ event: "userinfo", id: socket.id }));
}
wss.on("connection", function connection(socket) {
    socket.id = uuid.v4();
    console.log("Socket " + socket.id + " connected");
    //socket.onclose = socketOnClose.bind(this, socket);
    socket.on("message", function d(data) {
        messageReceived(socket, data);
    });
    socket.on("close", function () {
        removePlayer(socket);
    });

    if (game.p1 === null) {
        game.p1 = new Player(socket);
        sendUserInfo(socket);
        //game.p1.newPlayer(socket.id);
        //game.p1.sendDeck();
    } else {
        game.p2 = new Player(socket);
        sendUserInfo(socket);
        //game.p2.newPlayer(socket.id);
    }
    if (game.p1 != null && game.p2 != null) {
        /*  game.p2.newPlayer(game.p1.socket.id);
        game.p1.newPlayer(game.p2.socket.id);
        game.p2.generateDeck(decks.p2);
        game.p1.generateDeck(decks.p1);
        game.p1.sendDeck(game.p1.deck, false);
        game.p2.sendDeck(game.p2.deck, false);
        game.p1.sendDeck(game.p2.deck, true);
        game.p2.sendDeck(game.p1.deck, true);*/
        game.generateDecks(decks.p1, decks.p2);
        game.init();
    }

    // players.push(player);

    //socket.send(JSON.stringify({ event: "userinfo", id: socket.id }));
    /*  players.push(new Player(socket));
    players[0].deck.push(new Card(0, socket.id, "hanusa_radiance_elemental", "Angel Command", 9500, 7, 4, "light"));
    players[0].sendCard(players[0].deck[0]);*/
    /*socket.send(JSON.stringify({ event: "userinfo", id: socket.id }));
  if (players.length === 2) {
    var lobby = new Lobby(players[0], players[1]);
    lobby.init();
  }*/
});

function removePlayer(socket) {
    if (game.p1.socket != null) {
        if (socket.id === game.p1.socket.id) {
            game.p1 = null;
        }
    }
    if (game.p2.socket != null) {
        if (socket.id === game.p2.socket.id) {
            game.p2 = null;
        }
    }
}

function messageReceived(socket, d) {
    data = null;
    try {
        data = JSON.parse(d);
    } catch (e) {
        console.log("Data parsing failed!");
        return;
    }
    switch (data.event) {
        case "requestcard":
            game.drawCard(data.id);
            break;
        case "cheat":
            socket.send(
                JSON.stringify({
                    event: "cheat",
                    p1: { id: game.p1.socket.id, hand: game.p1.cards["hand"] },
                    p2: { id: game.p2.socket.id, hand: game.p2.cards["hand"] },
                })
            );
        default:
            break;
    }
}
