class Player {
    constructor(socketid) {
        this.id = socketid;
        this.deck = {
            hand: [],
            graveyard: [],
            mana: [],
            battlezone: [],
            deck: [],
            shield: [],
        };
        this.temp = {
            card: null,
            index: null,
        };
    }
    setUpDeck(deck) {
        this.deck["deck"] = [];
        var cards = this.deck["deck"];
        for (var i = 0; i < deck["deck"].length; i++) {
            cards.push(new Card(deck["deck"][i].id, deck["deck"][i].owner, deck["deck"][i].name));
        }
        this.sortDeck(this.id);
    }
    requestCard(p) {
        //if (socket != null) {
        var onDeck = false;
        for (var i = 0; i < this.deck["deck"].length; i++) {
            if (this.deck["deck"][i].isPointWithin(p)) {
                onDeck = true;
                break;
            }
        }
        if (onDeck) socket.send(JSON.stringify({ event: events.requestcard, id: this.id }));
        //}
    }

    getCardFrom(p, from) {
        for (let i = 0; i < this.deck[from].length; i++) {
            if (this.deck[from][i].isPointWithin(p)) {
                return this.deck[from][i];
            }
        }
    }
    getCard(p) {
        for (var key in this.deck) {
            var array = this.deck[key];
            for (var i = 0; i < array.length; i++) {
                if (array[i].isPointWithin(p)) {
                    return array[i];
                }
            }
        }
    }
    sortDeck(id) {
        var deck = this.deck["deck"];
        for (var i = 0; i < deck.length; i++) {
            //deck.push(new Card(new Point(0, 0), 130, 180, deckToCopy[i].image, deckToCopy[i]));
            deck[i].x = 3100 + i * 0.4;
            deck[i].y = 1700 - i * 0.6;
            if (id !== player.id) {
                var mirror = mirrorPosition(deck[i].x, deck[i].y);
                deck[i].x = mirror.x;
                deck[i].y = mirror.y;
            }

            //ctx.resetTransform();
            //     deck[i].id = i;
            //    deck[i].index = i;
        }
    }
    moveCard(from, to, id, name) {
        var temp;
        for (var i = 0; i < this.deck[from].length; i++) {
            if (this.deck[from][i].id === id) {
                temp = this.deck[from][i];
                temp.image = cards[name];
                this.deck[from].pop();
                this.deck[to].push(temp);
                break;
            }
        }
        temp = null;
        if (to === "hand") {
            this.organizeHand();
        }
    }

    dragStart(p) {
        var temp = null;
        if (this.deck["hand"].length < 9) {
            temp = this.getCardFrom(p, "hand");
        } else {
            for (let i = 0; i < this.deck["hand"].length; i++) {
                if (i === this.deck["hand"].length - 1) {
                    if (
                        p.x > this.deck["hand"][i].x &&
                        p.x < this.deck["hand"][i].x + this.deck["hand"][i].w &&
                        p.y > this.deck["hand"][i].y &&
                        p.y < this.deck["hand"][i].y + this.deck["hand"][i].h
                    ) {
                        temp = this.deck["hand"][i];
                    }
                } else {
                    if (
                        p.x > this.deck["hand"][i].x &&
                        p.x < this.deck["hand"][i + 1].x &&
                        p.y > this.deck["hand"][i].y &&
                        p.y < this.deck["hand"][i].y + this.deck["hand"][i].h
                    ) {
                        temp = this.deck["hand"][i];
                    }
                }
            }
        }
        if (temp != null) {
            this.temp.card = temp;
            if (this.temp.card != null) {
                for (var i = 0; i < this.deck["hand"].length; i++) {
                    if (this.deck["hand"][i] === temp) {
                        this.deck["hand"].splice(i, 1);
                        this.temp.index = i;
                    }
                }
                this.temp.card.x = p.x - this.temp.card.w / 2;
                this.temp.card.y = p.y - this.temp.card.h / 2;
            }
            this.organizeHand();
        }
    }
    drag(p) {
        this.temp.card.x = p.x - this.temp.card.w / 2;
        this.temp.card.y = p.y - this.temp.card.h / 2;
    }
    dragEnd(p) {
        if (this.temp.card != null) {
            this.deck["hand"].splice(this.temp.index, 0, this.temp.card);
            this.temp = { card: null, index: null };
        }
        this.organizeHand();
    }
    organizeHand() {
        var length = this.deck["hand"].length;
        var hand = this.deck["hand"];
        var x;
        var y = 1760;
        if (length <= 8) {
            var gap = 20;
            x = (3840 - (260 * length + gap * (length - 1))) / 2;
        } else {
            var gap = (-1 * ((length - 8) * 260)) / (length - 1);
            x = (3840 - (260 * length + gap * (length - 1))) / 2;
            var gap = (2220 - hand.length * 260) / (hand.length - 1);
            x = 810;
        }
        for (var i = 0; i < hand.length; i++) {
            hand[i].x = x + i * (gap + 260);
            if (hand[i].owner === player.id) {
                hand[i].y = y;
            } else {
                var mirror = mirrorPosition(hand[i].x, y);
                hand[i].x = mirror.x;
                hand[i].y = mirror.y;
            }
        }
    }
    drawBigCard() {
        if (this.temp.card === null) {
            var temp = null;
            if (this.deck["hand"].length < 9) {
                temp = this.getCardFrom(mousePos, "hand");
            } else {
                for (let i = 0; i < this.deck["hand"].length; i++) {
                    if (i === this.deck["hand"].length - 1) {
                        if (
                            mousePos.x > this.deck["hand"][i].x &&
                            mousePos.x < this.deck["hand"][i].x + this.deck["hand"][i].w &&
                            mousePos.y > this.deck["hand"][i].y &&
                            mousePos.y < this.deck["hand"][i].y + this.deck["hand"][i].h
                        ) {
                            temp = this.deck["hand"][i];
                        }
                    } else {
                        if (
                            mousePos.x > this.deck["hand"][i].x &&
                            mousePos.x < this.deck["hand"][i + 1].x &&
                            mousePos.y > this.deck["hand"][i].y &&
                            mousePos.y < this.deck["hand"][i].y + this.deck["hand"][i].h
                        ) {
                            temp = this.deck["hand"][i];
                        }
                    }
                }
            }

            if (temp != null) temp.mouseOnCard();
        }
    }

    update(ctx) {
        for (var key in this.deck) {
            var array = this.deck[key];
            for (var i = 0; i < array.length; i++) {
                array[i].update(ctx);
            }
        }
        this.drawBigCard();
        if (this.temp.card != null) {
            this.temp.card.update(ctx);
        }
    }
}
