/*const url = "ws://localhost:8010";



socket.onopen = socketOnOpen;
socket.onmessage = socketOnMessage;*/
/*socket.onerror = socketOnError;
socket.onclose = socketOnClose;
socket.onmessage = socketOnMessage;
*/

var socket = null;
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
const log = console.log;
window.onload = adjustResolution();
window.addEventListener("resize", adjustResolution);

document.addEventListener("mousemove", onMouseMove, false);
document.addEventListener("mousedown", onMouseDown, false);
document.addEventListener("mouseup", onMouseUp, false);

const events = {
    userinfo: "userinfo",
    gamestart: "gamestarted",
    sendcard: "sendcard",
    newplayer: "newplayer",
    receivedecks: "receivedecks",
    receiveplayers: "receiveplayers",
    requestcard: "requestcard",
    movecard: "movecard",
    cheat: "cheat",
};
canvas.width = settings.canvasWidth;
canvas.height = settings.canvasHeight;

var mousePos = {
    x: 0,
    y: 0,
};

var deck = [];

var player = null;
var opponent = null;
var tempCard = null;

//var screenFactor = settings.canvasWidth / 3840;
var screenFactor = settings.canvasWidth / window.innerWidth;

var img = new Image();

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect(), // abs. size of element
        scaleX = canvas.width / rect.width, // relationship bitmap vs. element for X
        scaleY = canvas.height / rect.height; // relationship bitmap vs. element for Y

    return {
        x: (evt.clientX - rect.left) * scaleX, // scale mouse coordinates after they have
        y: (evt.clientY - rect.top) * scaleY, // been adjusted to be relative to element
    };
}

// function getCard(player) {}

function onMouseDown(e) {
    if (player != null) {
        player.dragStart(mousePos);
    }
}

function onMouseUp(e) {
    if (player != null) {
        player.dragEnd(mousePos);
        player.requestCard(mousePos);
    }
}

function onMouseMove(e) {
    var pos = getMousePos(canvas, e);
    mousePos.x = pos.x;
    mousePos.y = pos.y;
    if (player != null) {
        if (player.temp.card != null) {
            player.drag(mousePos);
        }
    }
}

function adjustResolution() {
    if (window.innerWidth / 16 < window.innerHeight / 9) {
        canvas.style.width = window.innerWidth + "px";
        canvas.style.height = (window.innerWidth / 16) * 9 + "px";
    } else {
        canvas.style.height = window.innerHeight + "px";
        canvas.style.width = (window.innerHeight / 9) * 16 + "px";
    }
}

function socketOnOpen() {
    $("#play").hide();
    //console.log("connected");
    // socket.send('Connected'
}

function cheat() {
    this.socket.send(JSON.stringify({ event: "cheat" }));
}

function startGame() {
    const url = "ws://localhost:8010";

    socket = new WebSocket(url);

    socket.onopen = socketOnOpen;
    socket.onmessage = socketOnMessage;
    //$("#play").hide();
}

function mirrorPosition(x, y) {
    var posX = settings.canvasWidth - x - 260;
    var posY = settings.canvasHeight - y - 364;
    return { x: posX, y: posY };
}

function socketOnMessage(e) {
    data = JSON.parse(e.data);
    switch (data.event) {
        case events.userinfo:
            player = new Player(data.id);
            console.log("Player id: " + player.id);
            break;
        case events.gamestart:
            break;
        case events.cheat:
            if (data.p1.id === player.id) {
                for (var i = 0; i < data.p2.hand.length; i++) {
                    opponent.deck.hand[i].image = cards[data.p2.hand[i].name];
                }
            } else {
                for (var i = 0; i < data.p1.hand.length; i++) {
                    opponent.deck.hand[i].image = cards[data.p1.hand[i].name];
                }
            }
            break;
        case events.sendcard:
            /*img.src = `./img/cards/${data.card.id}.png`;*/
            /*socket deck.push(new Card(data.card.id, data.card.name));
            update();*/
            break;
        case events.receiveplayers:
            /*if (player === null) {
                player = new Player(data.id);
            } else {
                opponent = new Player(data.id);
            }*/
            if (data.p1.id === player.id) {
                opponent = new Player(data.p2.id);
            } else {
                opponent = new Player(data.p1.id);
            }
            //console.log("asd");
            /*log(player);
            log(opponent);*/
            break;
        case events.receivedecks:
            /*console.log(data);
            console.log(player.id);*/
            if (data.p1.id === player.id) {
                if (player != null) player.setUpDeck(data.p1.deck);
                if (opponent != null) opponent.setUpDeck(data.p2.deck);
            } else {
                if (player != null) player.setUpDeck(data.p2.deck);
                if (opponent != null) opponent.setUpDeck(data.p1.deck);
            }
            update();
            break;
        case events.movecard:
            if (data.playerid === player.id) {
                player.moveCard(data.from, data.to, data.id, data.name);
            } else {
                opponent.moveCard(data.from, data.to, data.id, data.name);
            }
            break;
        default:
        // console.log("Error no eventasdasasdad found");
    }
}

function update() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (player != null) player.update(ctx);
    if (opponent != null) opponent.update(ctx);
    requestAnimationFrame(update);
}
