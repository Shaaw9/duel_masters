var cards = {
    unknown: "./img/000/cardBack.png",
    aqua_hulcus: "./img/cards/aqua_hulcus.png",
    aqua_knight: "./img/cards/aqua_knight.png",
    aqua_sniper: "./img/cards/aqua_sniper.png",
    aqua_soldier: "./img/cards/aqua_soldier.png",
    aqua_vehicle: "./img/cards/aqua_vehicle.png",
    armored_walker_urherion: "./img/cards/armored_walker_urherion.png",
    artisan_picora: "./img/cards/artisan_picora.png",
    astrocomet_dragon: "./img/cards/astrocomet_dragon.png",
    aura_blast: "./img/cards/aura_blast.png",
    black_feather_shadow_of_rage: "./img/cards/black_feather_shadow_of_rage.png",
    bloody_squito: "./img/cards/bloody_squito.png",
    bolshack_dragon: "./img/cards/bolshack_dragon.png",
    bone_assassin_the_ripper: "./img/cards/bone_assassin_the_ripper.png",
    brain_serum: "./img/cards/brain_serum.png",
    brawler_zyler: "./img/cards/brawler_zyler.png",
    bronze_arm_tribe: "./img/cards/bronze_arm_tribe.png",
    burning_mane: "./img/cards/burning_mane.png",
    burning_power: "./img/cards/burning_power.png",
    candy_drop: "./img/cards/candy_drop.png",
    chaos_strike: "./img/cards/chaos_strike.png",
    chilias_the_oracle: "./img/cards/chilias_the_oracle.png",
    coiling_vines: "./img/cards/coiling_vines.png",
    creeping_plague: "./img/cards/creeping_plague.png",
    crimson_hammer: "./img/cards/crimson_hammer.png",
    crystal_memory: "./img/cards/crystal_memory.png",
    dark_clown: "./img/cards/dark_clown.png",
    dark_raven_shadow_of_grief: "./img/cards/dark_raven_shadow_of_grief.png",
    dark_reversal: "./img/cards/dark_reversal.png",
    deadly_fighter_braid_claw: "./img/cards/deadly_fighter_braid_claw.png",
    death_smoke: "./img/cards/death_smoke.png",
    deathblade_beetle: "./img/cards/deathblade_beetle.png",
    deathliger_lion_of_chaos: "./img/cards/deathliger_lion_of_chaos.png",
    dia_nork_moonlight_guardian: "./img/cards/dia_nork_moonlight_guardian.png",
    dimension_gate: "./img/cards/dimension_gate.png",
    dome_shell: "./img/cards/dome_shell.png",
    draglide: "./img/cards/draglide.png",
    emerald_grass: "./img/cards/emerald_grass.png",
    explosive_fighter_ucarn: "./img/cards/explosive_fighter_ucarn.png",
    faerie_child: "./img/cards/faerie_child.png",
    fatal_attacker_horvath: "./img/cards/fatal_attacker_horvath.png",
    fear_fang: "./img/cards/fear_fang.png",
    fire_sweeper_burning_hellion: "./img/cards/fire_sweeper_burning_hellion.png",
    forest_hornet: "./img/cards/forest_hornet.png",
    frei_vizier_of_air: "./img/cards/frei_vizier_of_air.png",
    gatling_skyterror: "./img/cards/gatling_skyterror.png",
    ghost_touch: "./img/cards/ghost_touch.png",
    gigaberos: "./img/cards/gigaberos.png",
    gigagiele: "./img/cards/gigagiele.png",
    gigargon: "./img/cards/gigargon.png",
    golden_wing_striker: "./img/cards/golden_wing_striker.png",
    gran_gure_space_guardian: "./img/cards/gran_gure_space_guardian.png",
    hanusa_radiance_elemental: "./img/cards/hanusa_radiance_elemental.png",
    holy_awe: "./img/cards/holy_awe.png",
    hunter_fish: "./img/cards/hunter_fish.png",
    iere_vizier_of_bullets: "./img/cards/iere_vizier_of_bullets.png",
    illusionary_merfolk: "./img/cards/illusionary_merfolk.png",
    immortal_baron_vorg: "./img/cards/immortal_baron_vorg.png",
    iocant_the_oracle: "./img/cards/iocant_the_oracle.png",
    king_coral: "./img/cards/king_coral.png",
    king_depthcon: "./img/cards/king_depthcon.png",
    king_ripped_hide: "./img/cards/king_ripped_hide.png",
    la_ura_giga_sky_guardian: "./img/cards/la_ura_giga_sky_guardian.png",
    lah_purification_enforcer: "./img/cards/lah_purification_enforcer.png",
    laser_wing: "./img/cards/laser_wing.png",
    lok_vizier_of_hunting: "./img/cards/lok_vizier_of_hunting.png",
    magma_gazer: "./img/cards/magma_gazer.png",
    marine_flower: "./img/cards/marine_flower.png",
    masked_horror_shadow_of_scorn: "./img/cards/masked_horror_shadow_of_scorn.png",
    meteosaur: "./img/cards/meteosaur.png",
    miele_vizier_of_lightning: "./img/cards/miele_vizier_of_lightning.png",
    mighty_shouter: "./img/cards/mighty_shouter.png",
    moonlight_flash: "./img/cards/moonlight_flash.png",
    natural_snare: "./img/cards/natural_snare.png",
    night_master_shadow_of_decay: "./img/cards/night_master_shadow_of_decay.png",
    nomad_hero_gigio: "./img/cards/nomad_hero_gigio.png",
    onslaughter_triceps: "./img/cards/onslaughter_triceps.png",
    pangaeas_song: "./img/cards/pangaeas_song.png",
    phantom_fish: "./img/cards/phantom_fish.png",
    poisonous_dahlia: "./img/cards/poisonous_dahlia.png",
    poisonous_mushroom: "./img/cards/poisonous_mushroom.png",
    rayla_truth_enforcer: "./img/cards/rayla_truth_enforcer.png",
    red_eye_scorpion: "./img/cards/red_eye_scorpion.png",
    reusol_the_oracle: "./img/cards/reusol_the_oracle.png",
    revolver_fish: "./img/cards/revolver_fish.png",
    roaring_great_horn: "./img/cards/roaring_great_horn.png",
    rothus_the_traveler: "./img/cards/rothus_the_traveler.png",
    ruby_grass: "./img/cards/ruby_grass.png",
    saucer_head_shark: "./img/cards/saucer_head_shark.png",
    scarlet_skyterror: "./img/cards/scarlet_skyterror.png",
    seamine: "./img/cards/seamine.png",
    senatine_jade_tree: "./img/cards/senatine_jade_tree.png",
    skeleton_soldier_the_defiled: "./img/cards/skeleton_soldier_the_defiled.png",
    solar_ray: "./img/cards/solar_ray.png",
    sonic_wing: "./img/cards/sonic_wing.png",
    spiral_gate: "./img/cards/spiral_gate.png",
    stampeding_longhorn: "./img/cards/stampeding_longhorn.png",
    steel_smasher: "./img/cards/steel_smasher.png",
    stinger_worm: "./img/cards/stinger_worm.png",
    stonesaur: "./img/cards/stonesaur.png",
    storm_shell: "./img/cards/storm_shell.png",
    super_explosive_volcanodon: "./img/cards/super_explosive_volcanodon.png",
    swamp_worm: "./img/cards/swamp_worm.png",
    szubs_kin_twilight_guardian: "./img/cards/szubs_kin_twilight_guardian.png",
    teleportation: "./img/cards/teleportation.png",
    terror_pit: "./img/cards/terror_pit.png",
    thorny_mandra: "./img/cards/thorny_mandra.png",
    toel_vizier_of_hope: "./img/cards/toel_vizier_of_hope.png",
    tornado_flame: "./img/cards/tornado_flame.png",
    tower_shell: "./img/cards/tower_shell.png",
    tri_horn_shepherd: "./img/cards/tri_horn_shepherd.png",
    tropico: "./img/cards/tropico.png",
    ultimate_force: "./img/cards/ultimate_force.png",
    unicorn_fish: "./img/cards/unicorn_fish.png",
    urth_purifying_elemental: "./img/cards/urth_purifying_elemental.png",
    vampire_silphy: "./img/cards/vampire_silphy.png",
    virtual_tripwire: "./img/cards/virtual_tripwire.png",
    wanderning_braineater: "./img/cards/wanderning_braineater.png",
    writhing_bone_ghoul: "./img/cards/writhing_bone_ghoul.png",
    zagaan_knight_of_darkness: "./img/cards/zagaan_knight_of_darkness.png",
};

importCards();
function importCards() {
    var loadProgress = 0;
    var numberOfImages = Object.keys(cards).length;
    var loadedImages = 0;
    Object.keys(cards).forEach(function (key, index) {
        var image = new Image();
        image.src = cards[key];
        image.onload = function () {
            cards[key] = createOffscreenCanvas(image, 260, 364);
            loadedImages++;
            loadProgress = loadedImages / numberOfImages;
            if (loadProgress === 1) {
            }
        };
    });
}

function createOffscreenCanvas(image, newWidth, newHeight, filterParameter = "none") {
    var width;
    var height;
    if (newWidth != null) {
        width = newWidth;
    } else {
        width = image.width;
    }
    if (newHeight != null) {
        height = newHeight;
    } else {
        height = image.height;
    }

    var offscreenCanvas = document.createElement("canvas"); // we prerender the resized ball image onto an offscreenCanvas to increase performance
    offscreenCanvas.width = width;
    offscreenCanvas.height = height;
    var offscreenCtx = offscreenCanvas.getContext("2d");
    offscreenCtx.filter = filterParameter;
    offscreenCtx.drawImage(image, 0, 0, width, height);
    //ctx.drawImage(offscreenCanvas, 0, 0);
    //console.log("OFFSCREENCANVAS")
    return offscreenCanvas;
}
