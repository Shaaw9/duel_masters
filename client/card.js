class Card {
    constructor(id, owner, name) {
        this.id = id;
        this.owner = owner;
        this.image = cards[name];
        this.x = 0;
        this.y = 0;
        this.w = this.image.width;
        this.h = this.image.height;
    }
    update(ctx) {
        if (this.owner === player.id) {
            ctx.drawImage(this.image, this.x, this.y);
        } else {
            ctx.save();
            ctx.translate(this.x, this.y);
            ctx.rotate((Math.PI / 180) * 180);
            ctx.drawImage(this.image, -this.w, -this.h);
            ctx.restore();
        }
    }

    mouseOnCard() {
        //  var boolean = this.isPointWithin(mousePos, "bool");
        //if (boolean) {
        ctx.drawImage(this.image, this.x - this.w / 2, this.y - 400, this.w * 2, this.h * 2);
        // }
    }

    isPointWithin(p, type = "card") {
        if (p.x > this.x && p.x < this.x + this.w && p.y > this.y && p.y < this.y + this.h) {
            if (type === "bool") {
                return true;
            } else {
                return this;
            }
        }
        return false;
    }
}
